<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Security\Http\EntryPoint;

use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Http\EntryPoint\FormAuthenticationEntryPoint;

class AuthenticationEntryPoint implements AuthenticationEntryPointInterface
{
    /** @var FormAuthenticationEntryPoint */
    protected $formAuthEntryPoint;

    /**
     * AuthenticationEntryPoint constructor.
     *
     * @param FormAuthenticationEntryPoint $formAuthEntryPoint
     */
    public function __construct(FormAuthenticationEntryPoint $formAuthEntryPoint)
    {
        $this->formAuthEntryPoint = $formAuthEntryPoint;
    }

    /**
     * Starts the authentication scheme.
     *
     * @param Request                 $request       The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->formAuthEntryPoint->start($request, $authException);
        }
        $array = array('success' => false);
        $response = new Response(json_encode($array), 401);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
