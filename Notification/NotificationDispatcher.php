<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Notification;

use Monolog\Logger;

/**
 * Class NotificationDispatcher.
 */
class NotificationDispatcher
{
    /**
     * @var NotificationRegistry
     */
    protected $registry;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * NotificationDispatcher constructor.
     *
     * @param NotificationRegistry $registry
     * @param Logger               $logger
     * @param \Twig_Environment    $twig
     */
    public function __construct(NotificationRegistry $registry, Logger $logger, \Twig_Environment $twig)
    {
        $this->registry = $registry;
        $this->logger = $logger;
        $this->twig = $twig;
    }

    /**
     * @param $type
     * @param int|null $level
     */
    public function dispatch($type, $subject, $level = null)
    {
        $notification = $this->registry->getType($type);
        $notification->setSubject($subject);

        $context = $notification->getContext();
        $context['notification'] = $notification;

        $this->logger->addRecord($level ? $level : $notification->getLevel(), $notification->getMessage(), $context);
    }
}
