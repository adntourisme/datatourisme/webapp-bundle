<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Notification\Handler;

use Datatourisme\Bundle\WebAppBundle\Notification\NotificationTypeInterface;
use Doctrine\ORM\EntityManager;
use Monolog\Handler\AbstractProcessingHandler;

/**
 * Class DoctrineORMHandler.
 */
class DoctrineORMHandler extends AbstractProcessingHandler
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function setEntityManager($em)
    {
        $this->em = $em;
    }

    protected function write(array $record)
    {
        $context = $record['context'];
        if (!isset($context['notification'])) {
            return false;
        }

        /** @var NotificationTypeInterface $notification */
        $notification = $context['notification'];

        // get entity
        $entity = $notification->getEntity();
        if ($entity) {
            $entity->setMessage($record['message_html']);
            $entity->setPlainMessage($record['message']);
            $entity->setLevel($record['level']);
            $entity->setLevelName($record['level_name']);
            $entity->setDatetime($record['datetime']);

            if (isset($record['description'])) {
                $entity->setDescription($record['description']);
            }

            if (isset($record['route'])) {
                $entity->setRoute($record['route']);
                $entity->setRouteParams($record['route_parameters']);
            }

            $this->em->persist($entity);
            $this->em->flush();
        }
    }
}
