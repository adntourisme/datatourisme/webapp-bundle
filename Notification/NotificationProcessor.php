<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Notification;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class NotificationProcessor.
 */
class NotificationProcessor
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * NotificationProcessor constructor.
     *
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function __invoke($record)
    {
        $context = $record['context'];
        /** @var NotificationTypeInterface $notification */
        $notification = $record['context']['notification'];

        // compile message
        $template = $this->twig->createTemplate($notification->getMessage());
        $html = $template->render($context);
        $record['message_html'] = $html;
        $record['message_tpl'] = $notification->getMessage();

        // strip tags in message
        $filter = $this->twig->getFilter('striptags')->getCallable();
        $record['message'] = $filter($record['message_html']);

        // compile description
        if ($notification->getDescription()) {
            $template = $this->twig->createTemplate($notification->getDescription());
            $record['description'] = $template->render($context);
            $record['description_tpl'] = $notification->getDescription();
        }

        // compile route
        if ($notification->getRoute()) {
            $record['route'] = $notification->getRoute();
            $record['route_title'] = $notification->getRouteTitle();
            $record['route_parameters'] = $notification->getRouteParameters();
        }

        return $record;
    }
}
