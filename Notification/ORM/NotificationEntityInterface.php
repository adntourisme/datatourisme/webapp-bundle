<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Notification\ORM;

/**
 * Interface NotificationEntityInterface.
 */
interface NotificationEntityInterface
{
    public function setMessage($message);

    public function setPlainMessage($message);

    public function setLevel($level);

    public function setLevelName($name);

    public function setDatetime($datetime);

    public function setDescription($description);

    public function setRoute($route);

    public function setRouteParams($params);
}
