<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Yaml;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class DatatourismeWebAppExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        $files = [
            'services.yml',
            'form.yml',
            'security.yml',
            'task-manager.yml',
            'twig.yml',
            'notification.yml',
        ];

        foreach ($files as $file) {
            $loader->load($file);
        }

        $this->loadMailerConfig($config['mailer'], $container);
    }

    /**
     * Load mailer config.
     */
    private function loadMailerConfig(array $config, ContainerBuilder $container)
    {
        $container->setParameter('datatourisme_web_app.mailer.sender_name', $config['sender']['name']);
        $container->setParameter('datatourisme_web_app.mailer.sender_address', $config['sender']['address']);
        $container->setParameter('datatourisme_web_app.mailer.emails', $config['emails']);
        $container->setParameter('datatourisme_web_app.mailer.subject_template', @$config['subject_template']);
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        $config = Yaml::parse(file_get_contents(__DIR__.'/../Resources/config/config.yml'));
        foreach ($container->getExtensions() as $name => $extension) {
            if (isset($config[$name])) {
                $container->prependExtensionConfig($name, $config[$name]);
            }
        }
    }
}
