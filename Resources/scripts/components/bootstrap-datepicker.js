/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

'use strict';

var $ = require("jquery");

require('bootstrap-daterangepicker');
var moment = require('moment');
require('moment/locale/fr');

$.fn.initComponents.add(function(dom) {
    $('[data-datepicker]', dom).each(function () {
        var start = moment().subtract(29, 'days');
        var end = moment();
        var selector = $('[data-datepicker-role="selector"]', this);
        var inputStart = $('[data-datepicker-role="start"]', this);
        var inputEnd = $('[data-datepicker-role="end"]', this);
        var placeholder = $('span', selector);

        function cb(start, end) {
            inputStart.val(start.format('DD/MM/Y'));
            inputEnd.val(end.format('DD/MM/Y'));
            placeholder.text(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
        }

        var settings = $(this).data();
        settings = $.extend({
            startDate: start,
            endDate: end,
            autoApply: true,
            ranges: {
                '7 derniers jours': [moment().subtract(6, 'days'), moment()],
                '30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Mois courant': [moment().startOf('month'), moment().endOf('month')],
                'Mois précédent': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Année courante': [moment().startOf('year'), moment().endOf('year')]
            },
            'locale': {
                'format': 'DD/MM/YYYY',
                'separator': ' - ',
                'applyLabel': 'Appliquer',
                'cancelLabel': 'Annuler',
                'fromLabel': 'de',
                'toLabel': 'à',
                'customRangeLabel': 'Personnalisé',
                'weekLabel': 'S',
                'daysOfWeek': [
                    'Di',
                    'Lu',
                    'Ma',
                    'Me',
                    'Je',
                    'Ve',
                    'Sa'
                ],
                'monthNames': [
                    'Janvier',
                    'Février',
                    'Mars',
                    'Avril',
                    'Mai',
                    'Juin',
                    'Juillet',
                    'Août',
                    'Septembre',
                    'Octobre',
                    'Novembre',
                    'Décembre'
                ],
                'firstDay': 1
            },
            'showCustomRangeLabel': true,
            'alwaysShowCalendars': true,
            'minDate': moment("2016-01-01"),
            'maxDate': moment(),
            'linkedCalendars': false
        }, settings);

        selector.daterangepicker(settings, cb);
        cb(start, end);
    });
});