/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 

'use strict';

var $ = require("jquery");

$.fn.initComponents.add(function(dom) {
    $('.image-upload-preview', dom).each(function () {
        var imgHolder = $(".preview-holder", this);
        var deleteBtn = $("[data-toggle='delete']", this);
        var updateBtn = $("[data-toggle='update']", this);
        var deleteCheckbox = $("input[type='checkbox']", this);
        var inputFile = $("input[type='file']", this);

        inputFile.hide();
        deleteCheckbox.parents(".checkbox").hide();

        if(!$("img", imgHolder).attr('src')) {
            imgHolder.hide();
            deleteBtn.hide();
        }

        // click on remove file
        deleteBtn.on('click', function () {
            deleteCheckbox.attr("checked", "checked");
            imgHolder.hide();
            deleteBtn.hide();
            inputFile.val(null);
        });

        // click on change file button
        updateBtn.on('click', function () {
            inputFile.click();
        });

        // reload preview when changing image file
        inputFile.on('change', function () {
            // reset action buttons
            deleteBtn.show();
            deleteCheckbox.removeAttr("checked");

            //Get count of selected files
            var countFiles = $(this)[0].files.length;
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();

            if (extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof (FileReader) != "undefined") {
                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("img", imgHolder).attr('src', e.target.result);
                            imgHolder.show();
                        };
                        reader.readAsDataURL($(this)[0].files[i]);
                    }
                }
            }
        });
    })
});
