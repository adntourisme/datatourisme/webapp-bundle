/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 

'use strict';

var $ = require("jquery");
var formSelector = '.modal-body form:first';

$(function () {
    $(document).on('click.modal.data-api', '[data-toggle="modal-remote"]', function (e) {
        e.preventDefault();
        var a = $(this);
        var url = a.attr('href');
        $("body").addClass("modal-loading");

        $.ajax({
            async: true,
            type: "GET",
            url: url,
            success: success
        });

        function success(data) {
            var modal = $(data);

            // focus on first input
            modal.on('loaded.bs.modal', function () {
                $('.modal-body form:first', this).find('input[type!=hidden],textarea,select').first().focus();
            });

            // on show : trigger loaded.bs.modal
            modal.on('shown.bs.modal', function () {
                $(this).trigger('loaded.bs.modal');
            });

            // remove on hide
            modal.on('hidden.bs.modal', function () {
                $(this).remove();
            });

//            // attach success/error event
//            modal.on('success.bs.modal', function () {
//                eval(a.data('onSuccess'))
//            });
//            modal.on('error.bs.modal', function () {
//                eval(a.data('onError'))
//            });

            // prepare submit events
            modal.on('submit', formSelector, ajaxFormSubmit(modal));
            var submit = function () {
                // fake submit button to trigger validation
                $('<input type="submit">').hide().appendTo($(formSelector, modal)).click().remove();
            };
            // submit on text input enter
            modal.on('keypress', formSelector + ' input', function (e) {
                if (e.which == 13) {
                    submit();
                    return false;    //<---- Add this line
                }
            });
            // submit on click
            modal.on('click', '[data-submit]', submit);

            // handle modal navigation
            modal.on('click.modal.data-api', '[data-toggle="modal-navigation"]', ajaxNavigation(modal));

            // handle modal full height option
            modal.on('show.bs.modal', onShowBsModalFullHeight);

            // open modal
            modal.initComponents();
            modal.modal(a.data());
            $("body").removeClass("modal-loading");
        }
    });

    function onShowBsModalFullHeight(e) {
        var modal = $(e.target);
        var isFullHeight = modal.hasClass("modal-fmh") || modal.hasClass("modal-fh");
        if(isFullHeight) {
            var resizer = function() {
                setFullHeight(modal, modal.hasClass("modal-fh"));
            };
            resizer();
            $(window).resize(resizer);
            modal.on('hidden.bs.modal', function (e) {
                $(window).off("resize", resizer);
            });
        }
    }

    $(document).on('show.bs.modal', onShowBsModalFullHeight);
});

/**
 * Submit modal form
 *
 * @param modal
 */
function ajaxFormSubmit(modal) {
    return function (e) {
        var form = $(this);
        // disable submit button
        $("[data-submit]", modal).attr('disabled', true);
        $(modal).addClass("modal-loading");

        // add submit button value in submitted data if exists
        var data = new FormData(this);
        var submitButton = $(document.activeElement);
        if (submitButton.val().length > 0) {
            data.append(submitButton.attr('name'), submitButton.val());
        }

        // POST through ajax
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: data,
            processData: false,
            contentType: false,
            complete: function (xhr, status) {
                var response = xhr.responseText;
                var ct = xhr.getResponseHeader("content-type") || "";
                var cd = xhr.getResponseHeader("content-disposition") || "";

                if (ct.indexOf('json') > -1) {
                    response = JSON.parse(response);
                    modal.trigger((status == "success" ? 'success' : 'error') + '.bs.modal', response);
                    $(modal).modal('hide');
                    handleJsonResponse(response);
                } else if (cd.length > 0) {
                    modal.trigger('success' + '.bs.modal', response);
                    $(modal).modal('hide');
                    var cdParts = cd.split('filename="');
                    if (cdParts.length == 2) {
                        var filename = cdParts[1].slice(0, cdParts[1].length - 1);
                        handleFileResponse(response, ct, filename);
                    }
                } else {
                    var part = $('.modal-dialog', response);
                    if (part.length) {
                        $('.modal-dialog', modal).replaceWith(part)
                        part.initComponents();
                    } else {
                        // debug
                        document.write(response);
                        document.close();
                    }
                    modal.trigger('loaded.bs.modal');
                    // re-enable submit button
                    $("[data-submit]", modal).attr('disabled', false);
                    $(modal).removeClass("modal-loading");
                }
            }
        });

        // prevent default
        e.preventDefault();
        return false;
    }
}

/**
 * Navigation to another modal dialog
 *
 * @param modal
 */
function ajaxNavigation(modal) {
    return function (e) {
        var url = $(this).attr('href');
        $(modal).addClass("modal-loading");
        $.get(url, function (data) {
            var part = $(".modal-dialog", data);
            $(".modal-dialog", modal).replaceWith(part);
            part.initComponents();
            $(modal).removeClass("modal-loading");
        });
        // prevent default
        e.preventDefault();
        return false;
    }
}

/**
 * @param response
 */
function handleJsonResponse(response) {
    if (response.redirect) {
        if (!($.ajaxLoad && $.ajaxLoad(response.redirect))) {
            window.location.href = response.redirect;
        }
    } else if (response.reload) {
        var selector = response.selector ? response.selector : null;
        if (!($.ajaxLoad && $.ajaxLoad(window.location.href, selector, true))) {
            window.location.reload();
        }
    }
}

/**
 * Get a rseponse and download it in a file
 *
 * @param response
 * @param ct
 * @param filename
 */
function handleFileResponse(response, ct, filename) {
    var blob = new Blob([response], {type: ct});

    // IE
    if (typeof window.navigator.msSaveBlob !== 'undefined') {
        window.navigator.msSaveBlob(blob, filename);
    } else {
        var URL = window.URL || window.webkitURL;
        var downloadUrl = URL.createObjectURL(blob);
        if (filename) {
            // use HTML5 a[download] attribute to specify filename
            var a = document.createElement("a");
            // safari doesn't support this yet
            if (typeof a.download === 'undefined') {
                window.location = downloadUrl;
            } else {
                a.href = downloadUrl;
                a.download = filename;
                document.body.appendChild(a);
                a.click();
            }
        }
        setTimeout(function () {
            URL.revokeObjectURL(downloadUrl);
        }, 1); // cleanup
    }
}

/**
 * Set the set modal height
 * @param modal
 * @param force
 */
function setFullHeight(modal, force) {
    setTimeout(function() {
        var $modal          = $(modal);
        var $dialog         = $modal.find('.modal-dialog');
        var $content        = $modal.find('.modal-content');
        var $body           = $content.find('.modal-body');
        var dialogMarginTop = parseInt($dialog.css("marginTop"));
        var windowHeight    = $(window).height();
        var headerHeight    = $content.find('.modal-header').outerHeight() || 0;
        var footerHeight    = $content.find('.modal-footer').outerHeight() || 0;
        var availableHeight     = windowHeight - (dialogMarginTop * 2) - headerHeight - footerHeight;
        $body.css('overflow', 'auto');
        if(force) {
            $body.css('height', availableHeight);
        } else {
            $body.css('max-height', availableHeight);
        }
    }, 200);
}