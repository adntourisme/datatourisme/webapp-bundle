/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

'use strict';

var $ = require("jquery");

/**
 * data-visibility-target="..." - selector of the control upon which this controls visibility depends
 * data-visibility-action="show|hide" - Whether to show or hide, default is to show
 * data-visibility-value="..." - Comma-separated list of values upon which to show (or hide) the control
 */
$.fn.initComponents.add(function(dom) {
    $('[data-visibility-target]', dom).each(function () {
        var self = $(this);
        var targetId = self.data('visibilityTarget');
        var target = $(targetId);
        var tagType = target.prop("tagName");

        var action = self.data('visibilityAction');
        if (action == '' || action == null) {
            action = 'show';
        }

        var valueField = self.data('visibilityValue');
        var values = valueField != null ? String(valueField).split(',') : null;

        // SELECT
        if (tagType == 'SELECT') {
            // When the target changes
            var callback = function () {
                var value = target.val().toString();
                if(values == null) {
                    setVisibility(self, action, value != "");
                } else {
                    for (var i = 0; i < values.length; ++i) {
                        var _value = values[i];
                        if (value == _value) {
                            setVisibility(self, action, true);
                            return;
                        }
                    }
                    setVisibility(self, action, false);
                }
            };
            target.change(callback);
            return callback();
        }

        // CHECKBOX
        if (tagType == 'INPUT') {
            var inputType = target.attr('type');
            if (inputType == 'checkbox') {
                var callback = function () {
                    var isChecked = target.is(':checked');
                    setVisibility(self, action, isChecked);
                };
                target.click(callback);
                return callback();
            }
        }

        // DEFAULT
        var callback = function () {
            var value = target.val().toString();
            setVisibility(self, action, value != "");
        };
        target.change(callback);
        return callback();
    });
});

/**
 * @param elmt
 * @param action
 * @param bool
 */
function setVisibility(elmt, action, bool) {
    if (bool) {
        if (action == 'show') {
            elmt.show();
        } else if (action == 'hide') {
            elmt.hide();
        }
    } else {
        if (action == 'show') {
            elmt.hide();
        } else if (action == 'hide') {
            elmt.show();
        }
    }
}
