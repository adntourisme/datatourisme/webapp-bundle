<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Twig;

use Datatourisme\Bundle\WebAppBundle\Twig\Markdown\DatatourismeMarkdown;
use cebe\markdown\GithubMarkdown;

/**
 * Class MarkdownExtension.
 */
class MarkdownExtension extends \Twig_Extension
{
    /**
     * @var GithubMarkdown
     */
    private $markdownParser;

    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('markdownize', array($this, 'markdownizeFilter')),
        );
    }

    /**
     * @param string $str
     *
     * @return string
     */
    public function markdownizeFilter(string $str): string
    {
        if (!$this->markdownParser) {
            $markdownParser = new DatatourismeMarkdown();
            $markdownParser->enableNewlines = true;
            $markdownParser->html5 = true;
            $this->markdownParser = $markdownParser;
        }

        // We prevent XSS here because the template will have to |raw the output.
        return $this->markdownParser->parse(htmlspecialchars($str));
    }

}
