<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Twig;

use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserExtension.
 */
class UserExtension extends \Twig_Extension
{
    /**
     * @var RoleHierarchy
     */
    private $roleHierarchy;

    /**
     * UserExtension constructor.
     *
     * @param RoleHierarchy $roleHierarchy
     */
    public function __construct(RoleHierarchy $roleHierarchy)
    {
        $this->roleHierarchy = $roleHierarchy;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('has_role', array($this, 'hasRole')),
        );
    }

    /**
     * @param $role
     * @param UserInterface $user
     *
     * @return bool
     */
    public function hasRole($role, UserInterface $user, $strict = false)
    {
        if (!($role instanceof RoleInterface)) {
            $role = new Role($role);
        }

        if ($strict) {
            return in_array($role->getRole(), $user->getRoles());
        } else {
            foreach ($user->getRoles() as $userRole) {
                if (in_array($role, $this->roleHierarchy->getReachableRoles(array(new Role($userRole))))) {
                    return true;
                }
            }

            return false;
        }
    }
}
