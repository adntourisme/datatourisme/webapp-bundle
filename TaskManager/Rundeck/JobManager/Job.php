<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\JobManager;

use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\AbstractRundeck;
use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\Connection\ApiMapper;
use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\Connection\RundeckClient;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Job.
 */
class Job extends AbstractRundeck
{
    /**
     * @var RundeckClient
     * */
    protected $client;

    /**
     * @var string
     */
    public $id;

    /**
     * Job constructor.
     *
     * @param $client
     * @param string|null $id
     */
    public function __construct($client, $id = null)
    {
        $this->client = $client;
        $this->id = $id;
    }

    /**
     * Get job detail (schedule, command...).
     *
     * @return Job|bool
     *
     * @throws \Exception
     */
    public function getDetails()
    {
        if (!isset($this->id)) {
            throw new \Exception('An id must be given');
        }
        $resp = $this->client->request(
            'GET',
            '/api/1/job/'.$this->id, [
                'cookies' => $this->client->getCookieClient(),
            ]
        );

        $data = $this->decodeResponse($resp);

        if (!isset($data['job'])) {
            return false;
        }

        return $data['job'];
    }

    /**
     * Disable current job execution.
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function disable()
    {
        try {
            if (!isset($this->id)) {
                throw new \Exception('An id must be given');
            }
            $response = $this->client->request(
                'POST',
                '/api/14/job/'.$this->id.'/execution/disable', [
                    'cookies' => $this->client->getCookieClient(),
                ]
            );
            if ('204' == $response->getStatusCode() && 'No Content' == $response->getReasonPhrase()) {
                return true;
            }
        } catch (ClientException $e) {
            // $response = $e->getResponse();
            // $responseBodyAsString = $response->getBody()->getContents();
        }

        return false;
    }

    /**
     * Enable current job execution.
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function enable()
    {
        try {
            if (!isset($this->id)) {
                throw new \Exception('An id must be given');
            }
            $response = $this->client->request(
                'POST',
                '/api/14/job/'.$this->id.'/execution/enable', [
                    'cookies' => $this->client->getCookieClient(),
                ]
            );
            if ('204' == $response->getStatusCode() && 'No Content' == $response->getReasonPhrase()) {
                return true;
            }
        } catch (ClientException $e) {
            // $response = $e->getResponse();
            // $responseBodyAsString = $response->getBody()->getContents();
        }

        return false;
    }

    /**
     * Delete current job.
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            if (!isset($this->id)) {
                throw new \Exception('An id must be given');
            }
            $response = $this->client->request(
                'DELETE',
                '/api/1/job/'.$this->id, [
                    'cookies' => $this->client->getCookieClient(),
                ]
            );
            if ('204' == $response->getStatusCode() && 'No Content' == $response->getReasonPhrase()) {
                return true;
            }
        } catch (ClientException $e) {
            // $response = $e->getResponse();
            // $responseBodyAsString = $response->getBody()->getContents();
        }

        return false;
    }

    /**
     * Run current job.
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function run()
    {
        $resp = $this->client->request('GET', '/api/1/job/'.$this->id.'/run', [
            'cookies' => $this->client->getCookieClient(),
        ]);

        $data = $this->decodeResponse($resp);

        if (!isset($data['executions']['execution']['job'])) {
            throw new \Exception('rundeck execution not found');
        }
        if (isset($data['executions']['execution']['job']['@attributes'])) {
            $data['executions']['execution']['job'] = array($data['executions']['execution']['job']);
        }

        return (new ApiMapper($this->client))->getAllFromEncoded($data['executions']['execution']['job']);
    }

    /**
     * Transform object to YAML to be send to Project::addJob.
     *
     * @return string
     */
    public function toYAML()
    {
        $Yaml['name'] = (string) $this->name;
        $Yaml['description'] = $this->description ?? '';
        $Yaml['executionEnabled'] = $this->executionenabled ?? true;
        $Yaml['group'] = $this->group;
        $Yaml['loglevel'] = $this->loglevel ?? 'INFO';
        $Yaml['scheduleEnabled'] = $this->scheduleenabled ?? false;
        $Yaml['schedule'] = $this->schedule ?? array();
        $Yaml['sequence'] = $this->sequence ?? array();

        return Yaml::dump([$Yaml], 10);
    }

    /**
     * @param $m
     * @param $p
     *
     * @throws \Exception
     */
    public function __call($m, $p)
    {
        $v = strtolower(substr($m, 3));
        if (!strncasecmp($m, 'set', 3)) {
            $this->$v = $p[0];
        }
        if (!strncasecmp($m, 'get', 3)) {
            if (!isset($this->$v)) {
                throw new \Exception($v.' not set in Job class');
            }

            return $this->$v;
        }

        return null;
    }

    /**
     * Add a command to the job.
     *
     * @param $command
     */
    public function addCommand($command)
    {
        $this->sequence['keepgoing'] = true;
        $this->sequence['strategy'] = 'node-first';
        $this->sequence['commands'][] = array('exec' => $command);
    }

    /**
     * Add a url to call to the job.
     *
     * @param $url
     */
    public function addScriptUrl($url)
    {
        $this->sequence['keepgoing'] = true;
        $this->sequence['strategy'] = 'node-first';
        $this->sequence['commands'][] = array('exec' => 'curl -Ss '.$url);
    }

    /**
     * Schedule the job in a crontab format.
     *
     * @param $crontab
     */
    public function addSchedule($crontab)
    {
        $this->scheduleenabled = true;
        $this->schedule['crontab'] = $crontab;
    }
}
