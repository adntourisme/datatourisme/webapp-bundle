<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\Connection;

use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\AbstractRundeck;
use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\JobManager\Job;
use GuzzleHttp\Exception\ClientException;

/**
 * Class Project.
 */
class Project extends AbstractRundeck
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var RundeckClient
     */
    protected $client;

    /**
     * Project constructor.
     *
     * @param RundeckClient $client
     * @param string        $id
     */
    public function __construct($client, $id = 'Default')
    {
        $this->client = $client;
        $this->id = $id;
        if (!$this->exists()) {
            $this->create($this->id);
        }
    }

    /**
     * Check if project exists in rundeck.
     *
     * @return bool
     */
    public function exists()
    {
        $resp = $this->client->request(
            'GET',
            '/api/1/projects', [
                'cookies' => $this->client->getCookieClient(),
            ]
        );

        $data = $this->decodeResponse($resp);
        if (isset($data['projects']) && isset($data['projects']['project'])) {
            if ($data['projects']['@attributes']['count'] == 1) {
                $data['projects']['project'] = [$data['projects']['project']];
            }
            foreach ($data['projects']['project'] as $project) {
                if ($project['name'] == $this->id) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string $group
     *
     * @return Job[]
     */
    public function getJobList($group = '*')
    {
        $groupPath = 'groupPath='.$group;
        $resp = $this->client->request(
            'GET',
            '/api/14/project/'.$this->id.'/jobs?'.$groupPath, [
                'cookies' => $this->client->getCookieClient(),
            ]
        );

        $data = $this->decodeResponse($resp);

        if (!isset($data['job'])) {
            return [];
        }

        return (new ApiMapper($this->client))->getAllFromEncoded($data['job']);
    }

    /**
     * @param string $name
     *
     * @return Job | bool
     */
    public function getJobFromList($group, $name)
    {
        $listJobs = $this->getJobList($group);
        if (count($listJobs)) {
            foreach ($listJobs as $job) {
                if ($job->getName() == $name) {
                    return $job;
                }
            }
        }

        return false;
    }

    /**
     * Create a job in the project.
     *
     * @param Job    $job
     * @param string $mode
     *
     * @return Job|bool
     *
     * @throws \Exception
     */
    public function addJob($job, $mode = 'create')
    {
        try {
            $response = $this->client->request(
                'POST',
                '/api/14/project/'.$this->id."/jobs/import?fileformat=yaml&dupeOption=$mode", [
                    'cookies' => $this->client->getCookieClient(),
                    'headers' => ['Content-Type' => 'application/yaml'],
                    'body' => trim($job->toYAML()),
                ]
            );
            $data = $this->decodeResponse($response);
            if (isset($data['succeeded']) && isset($data['succeeded']['job'])) {
                $jobTasker = (new ApiMapper($this->client))->getFromEncoded($data['succeeded']['job']);

                return $jobTasker;
            }
            if (isset($data['failed']) && isset($data['failed']['job'])) {
                throw new \Exception($data['failed']['job']['error']);
            }
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            throw new \Exception($responseBodyAsString);
        }

        return false;
    }

    /**
     * Delete all job of a project.
     *
     * @param string $group
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function deleteAllJobs($group = '*')
    {
        $tabData = $this->getJobList($group);
        if (0 == count($tabData)) {
            return true;
        }
        foreach ($tabData as $data) {
            if (!$data->delete()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Create project if not exists.
     *
     * @param $name
     *
     * @throws \Exception
     */
    private function create($name)
    {
        try {
            $this->client->request(
                'POST',
                '/api/11/projects/', [
                    'cookies' => $this->client->getCookieClient(),
                    'headers' => ['Content-Type' => 'application/json'],
                    'body' => json_encode(['name' => $name]),
                ]
            );
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            throw new \Exception($responseBodyAsString);
        }
    }
}
