<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\Connection;

use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\JobManager\Job;

/**
 * Class ApiMapper.
 */
class ApiMapper
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var RundeckClient
     */
    protected $client;

    /**
     * JobApiMapper constructor.
     *
     * @param RundeckClient $client
     * @param string|null   $id
     */
    public function __construct(RundeckClient $client, $id = null)
    {
        $this->client = $client;
        $this->id = $id;
    }

    /**
     * @param array $params
     *
     * @return Job
     */
    public function getFromEncoded(array $params)
    {
        $job = new Job($this->client);
        foreach ($params as $key => $value) {
            if ('@attributes' == $key) {
                if (isset($params[$key]['id'])) {
                    $job->id = $params[$key]['id'];
                }
                continue;
            }
            $job->$key = $value;
        }

        return $job;
    }

    /**
     * @param array $encs
     *
     * @return array
     */
    public function getAllFromEncoded(array $encs)
    {
        $data = [];
        if (!isset($encs[0])) {
            return [$this->getFromEncoded($encs)];
        }
        foreach ($encs as $enc) {
            $data[] = $this->getFromEncoded($enc);
        }

        return $data;
    }
}
