<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\TaskManager;

use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\JobManager\JobManagerInterface;
use Leezy\PheanstalkBundle\Proxy\PheanstalkProxy;

/**
 * Class ScheduleTask.
 */
class TaskManager
{
    /**
     * @var BeanStalkMessenger
     */
    protected $beanStalkMessenger;

    /**
     * @var JobManagerInterface
     */
    protected $rundeckManager;

    /**
     * ScheduleTask constructor.
     *
     * @param BeanStalkMessenger  $beanStalkMessenger
     * @param JobManagerInterface $rundeckManager
     */
    public function __construct(BeanStalkMessenger $beanStalkMessenger, JobManagerInterface $rundeckManager)
    {
        $this->beanStalkMessenger = $beanStalkMessenger;
        $this->rundeckManager = $rundeckManager;
    }

    /**
     * Send message to beanstalk.
     *
     * @param $name
     * @param array|string|null $arguments
     * @param int               $priority
     *
     * @return int
     */
    public function run($name, $arguments = null, $priority = PheanstalkProxy::DEFAULT_PRIORITY)
    {
        return $this->beanStalkMessenger->sendMessage($name, $arguments, $priority);
    }

    /**
     * Add or update a rundeck task.
     *
     * @param $id
     * @param $name
     * @param $scriptUrl
     * @param int  $hour
     * @param null $description
     * @param bool $enable
     *
     * @return int
     */
    public function add($id, $name, $scriptUrl, $hour = -1, $description = null, $enable = false)
    {
        return $this->rundeckManager->addTask($id, $name, $scriptUrl, $hour, $description, $enable);
    }

    /**
     * Update a rundeck task.
     *
     * @param $id
     * @param $name
     * @param $scriptUrl
     * @param int  $hour
     * @param null $description
     * @param bool $enable
     *
     * @return int
     */
    public function update($id, $name, $scriptUrl, $hour = -1, $description = null, $enable = false)
    {
        return $this->add($id, $name, $scriptUrl, $hour, $description, $enable);
    }

    /**
     * Test if rundeck has the task.
     *
     * @param $id
     * @param $name
     *
     * @return bool
     */
    public function has($id, $name)
    {
        return $this->rundeckManager->hasTask($id, $name);
    }

    /**
     * Enable a task.
     *
     * @param $id
     * @param $name
     *
     * @return bool
     */
    public function enable($id, $name)
    {
        return $this->rundeckManager->enableTask($id, $name);
    }

    /**
     * Disable a task.
     *
     * @param $id
     * @param $name
     *
     * @return bool
     */
    public function disable($id, $name)
    {
        return $this->rundeckManager->disableTask($id, $name);
    }

    /**
     * Remove all task of the group.
     *
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        return $this->rundeckManager->deleteAllGroupTasks($id);
    }
}
