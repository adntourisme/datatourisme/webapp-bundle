<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Utils;

use League\Csv\CharsetConverter;
use League\Csv\Reader;
use League\Csv\Writer;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;


/**
 * Class CsvWriter.
 */
class CsvWriter
{
    /**
     * @var Writer
     */
    protected $writer;

    /**
     * @var PropertyAccessor
     */
    protected $propertyAccessor;

    /**
     * @var array
     */
    protected $fields;

    /**
     * CsvWriter constructor.
     */
    public function __construct($fields = [])
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        $this->writer = Writer::createFromFileObject(new \SplTempFileObject());
        $this->writer->setDelimiter(",");
        $this->writer->setOutputBOM(Reader::BOM_UTF8);
        $this->writer->setEnclosure("\"");
        //CharsetConverter::addTo($this->writer, 'UTF-16', 'ISO-8859-1');
        $this->setFields($fields);
    }

    /**
     * @param $fields
     */
    public function setFields($fields) {
        $this->fields = $fields;
        $this->writer->insertOne(array_keys($fields));
    }

    /**
     * @param $record
     */
    public function write($record) {
        $record = array_replace(array_flip(array_keys($this->fields)), $record);
        foreach($record as $key => $value) {
            if($value instanceof \DateTime) {
                //$value = $value->format("Y-m-d H:i:s");
                $value = $value->format("d/m/Y H:i:s");
            }
            $record[$key] = $value;
        }

        $this->writer->insertOne(array_values($record));
    }

    /**
     * @param $obj
     */
    public function writeObject($obj) {
        $record = [];
        foreach($this->fields as $key => $path) {
            if($path instanceof \Closure) {
                $record[$key] = $path($obj);
            } else {
                $record[$key] = $this->extractValue($obj, $path);
            }
        }
        $this->write($record);
    }

    /**
     * @param $objs
     */
    public function writeObjects($objs) {
        foreach($objs as $obj) {
            $this->writeObject($obj);
        }
    }

    /**
     * @param $filename
     */
    public function output($filename) {
        $this->writer->output($filename);
        exit;
    }

    /**
     * @param $objectOrArray
     * @param $propertyPath
     * @return mixed|null
     */
    public function extractValue($objectOrArray, $propertyPath) {
        try {
            return $this->propertyAccessor->getValue($objectOrArray, $propertyPath);
        } catch(UnexpectedTypeException $e) {
            return null;
        } catch(NoSuchPropertyException $e) {
            return null;
        }
    }
}
