<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\User\UserInterface;

class RoleType extends AbstractType
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RoleHierarchy
     */
    private $roleHierarchy;

    /**
     * UserVoter constructor.
     *
     * @param $roleHierarchy
     */
    public function __construct(TokenStorageInterface $tokenStorage, RoleHierarchy $roleHierarchy)
    {
        $this->tokenStorage = $tokenStorage;
        $this->roleHierarchy = $roleHierarchy;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return ChoiceType::class;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $roles = array();

        // get user reachable roles
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof UserInterface) {
            $roles = $this->roleHierarchy->getReachableRoles(array_map(function ($role) {
                return new Role($role);
            }, $user->getRoles()));

            $roles = array_map(function (Role $role) {
                return $role->getRole();
            }, $roles);

            $roles = array_combine($roles, $roles);
            $roles = array_reverse($roles);
        }

        $resolver
            ->setDefaults(array(
                'label' => 'Rôle',
                'choices' => $roles,
            ));

        $resolver->addAllowedTypes('choice_label', 'array');
        $resolver->setNormalizer('choice_label', function (Options $options, $labels) {
            if (is_array($labels)) {
                return function ($value, $key, $index) use ($labels) {
                    return isset($labels[$value]) ? $labels[$value] : $value;
                };
            }

            return $labels;
        });
    }
}
