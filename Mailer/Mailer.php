<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Mailer;

use Pelago\Emogrifier;
use Html2Text\Html2Text;
use Symfony\Component\Cache\Exception\InvalidArgumentException;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class Mailer.
 */
class Mailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var RequestContext
     */
    protected $context;

    /**
     * @var string
     */
    private $senderAddress;

    /**
     * @var string
     */
    private $senderName;

    /**
     * @var array
     */
    protected $configuration;

    /**
     * @var string
     */
    private $subjectTemplate;

    /**
     * Mailer constructor.
     *
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $twig
     * @param TranslatorInterface $translator
     * @param array $configuration
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, TranslatorInterface $translator, RequestContext $context, array $configuration)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->translator = $translator;
        $this->context = $context;
        $this->configuration = $configuration;
    }

    /**
     * @param string $address
     * @param string $name
     */
    public function setSender($address, $name = null)
    {
        $this->senderAddress = $address;
        $this->senderName = $name;
    }

    /**
     * @param string $subjectTemplate
     */
    public function setSubjectTemplate($subjectTemplate)
    {
        $this->subjectTemplate = $subjectTemplate;
    }

    /**
     * @param $code
     * @param $recipient
     * @param array $data
     * @param array $excludes
     *
     * @return int
     */
    public function send($code, $recipient, array $data = [], array $excludes = [])
    {
        if ($recipient instanceof MailerRecipientsInterface) {
            $recipient = $recipient->getRecipients();
        }

        if (is_array($recipient) || $recipient instanceof \Traversable) {
            $success = 0;
            foreach ($recipient as $_recipient) {
                $success += $this->send($code, $_recipient, $data, $excludes);
            }

            return $success;
        }

        $locale = $this->translator->getLocale();

        if ($recipient instanceof MailerRecipientInterface) {
            $granted = $recipient->isMailingGranted($code, $data);
            if (!$granted || in_array($recipient, $excludes)) {
                return 0;
            }
            $data['recipient'] = $recipient;
            if(!empty($recipient->getLocale())) {
                $locale = $recipient->getLocale();
            }
            $recipient = $recipient->getEmail();
        }

        if(!\Swift_Validate::email($recipient)) {
            return 0;
        }

        if (in_array($recipient, $excludes)) {
            return 0;
        }

        $currentLocale = $this->translator->getLocale();
        $this->translator->setLocale($locale);
        if($this->context) {
            $this->context->setParameter("_locale", $locale);
        }
        $email = $this->getRenderedEmail($code, $data);
        $this->translator->setLocale($currentLocale);
        if($this->context) {
            $this->context->setParameter("_locale", $currentLocale);
        }

        if (!$email->isEnabled()) {
            return 0;
        }

        // precode the subject to avoid switfmailer bug
        // fix bug https://github.com/swiftmailer/swiftmailer/issues/665
        $subject = '=?UTF-8?B?'.base64_encode($email->getSubject()).'?=';
        $message = \Swift_Message::newInstance(null, null, 'text/html', null)
            ->setSubject($subject)
            ->setFrom($email->getSenderAddress(), $email->getSenderName())
            ->setTo([$recipient]);

        $message->setBody($email->getBody());
        $message->addPart(Html2Text::convert($email->getBody()), 'text/plain');

        return $this->mailer->send($message);
    }

    /**
     * @param string $code
     * @param array  $data
     *
     * @return Email
     */
    private function getRenderedEmail($code, array $data)
    {
        // load email from config
        $email = $this->getEmailFromConfiguration($code);

        // process template
        $data = $this->twig->mergeGlobals($data);
        $template = $this->twig->loadTemplate($email->getTemplate());
        $subject = $template->renderBlock('subject', $data);
        $html = $template->render($data);

        // inline css
        $emogrifier = new Emogrifier($html);
        $body = $emogrifier->emogrify();

        // alter email
        $email->setBody($body);
        if ($subject) {
            $email->setSubject($subject);
        }

        if ($this->subjectTemplate) {
            $email->setSubject(sprintf($this->subjectTemplate, $email->getSubject()));
        }

        return $email;
    }

    /**
     * @param string $code
     *
     * @return Email
     */
    private function getEmailFromConfiguration($code)
    {
        if (!isset($this->configuration[$code])) {
            throw new InvalidArgumentException(sprintf('Email with code "%s" does not exist!', $code));
        }

        $configuration = $this->configuration[$code];

        $email = new Email();
        $email->setSenderAddress($this->senderAddress);
        $email->setSenderName($this->senderName);
        $email->setCode($code);
        $email->setTemplate($configuration['template']);

        if (isset($configuration['subject'])) {
            $email->setSubject($configuration['subject']);
        }

        if (isset($configuration['enabled']) && false === $configuration['enabled']) {
            $email->setEnabled(false);
        }
        if (isset($configuration['sender']['name'])) {
            $email->setSenderName($configuration['sender']['name']);
        }
        if (isset($configuration['sender']['address'])) {
            $email->setSenderAddress($configuration['sender']['address']);
        }

        return $email;
    }
}
